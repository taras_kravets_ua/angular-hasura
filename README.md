## Setup db locally
1. Copy example.env to a new file called .env
2. Update the project folder env value with your system path 
3. Read environment variables `source .env`
4. Run `docker-compose down && docker-compose up --build --force-recreate --no-deps`

