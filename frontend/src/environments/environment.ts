export const environment = {
  production: false,
  apiUrl: 'http://localhost:8080/v1/graphql',
  wsUri: 'ws://localhost:8080/v1/graphql',
  hasuraAdminSecret: 'admin-secret-dev',
};
