import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { GraphQLModule } from './graphql.module';

import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { split, ApolloClientOptions, InMemoryCache } from '@apollo/client/core';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    FontAwesomeModule,
    GraphQLModule
  ],
  bootstrap: [AppComponent],
  providers: [{
    provide: APOLLO_OPTIONS,
    // @ts-ignore
    useFactory(httpLink: HttpLink): ApolloClientOptions {
      const http = httpLink.create({
        uri: environment.apiUrl,
      });

      const ws = new WebSocketLink({
        uri: environment.wsUri,
        options: {
          reconnect: true,
          connectionParams: {
            headers: {'x-hasura-admin-secret': environment.hasuraAdminSecret}
          }
        },
      });

      const link = split(
        ({query}) => {
          // @ts-ignore
          const {kind, operation} = getMainDefinition(query);
          return (
            kind === 'OperationDefinition' && operation === 'subscription'
          );
        },
        ws,
        http,
      );

      return {
        link,
        cache: new InMemoryCache(),
      };
    },
    deps: [HttpLink],
  }]
})
export class AppModule { }
