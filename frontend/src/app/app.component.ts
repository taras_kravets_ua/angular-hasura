import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CreateFormComponent } from './shared/components/create-form/create-form.component';
import { Item, ItemService } from './services/item.service';
import { SnackbarService } from './services/snackbar.service';
import { ApolloService } from './services/apollo.service';

enum Message {
  Created = 'Item has been created',
  NotCreated = `Item hasn't been created`,
  Deleted = `Item has been deleted`,
  NotDeleted = `Item hasn't been created`
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private subs: Subscription[] = [];

  constructor(
    public dialog: MatDialog,
    public apolloService: ApolloService,
    private itemService: ItemService,
    private snackbarService: SnackbarService,
  ) {}

  ngOnInit(): void {
    this.apolloService.makeSubscriptionOnItems();
  }

  ngOnDestroy(): void {
    this.subs.map(sub => sub.unsubscribe());
  }

  showCreationDialog(): void {
    const dialogRef = this.dialog.open(CreateFormComponent);

    const dialogSub = dialogRef.afterClosed().pipe(
      switchMap((newItem: Item) => this.itemService.createItem(newItem))
    ).subscribe((res: any) => {
      if (res.data) {
        this.showSuccessMessage(Message.Created);
      } else {
        this.showErrorMessage(Message.NotCreated);
      }
    });

    this.subs.push(dialogSub);
  }

  removeItem(item: Item): void {
    this.itemService.deleteItemById(item).subscribe((res: any) => {
      if (res.data) {
        this.showSuccessMessage(Message.Deleted);
      } else {
        this.showErrorMessage(Message.NotDeleted);
      }
    });
  }

  private showSuccessMessage(message: string): void {
    this.snackbarService.showMessage(message);
  }

  private showErrorMessage(message: string): void {
    this.snackbarService.showMessage(message, 'Close');
  }
}
