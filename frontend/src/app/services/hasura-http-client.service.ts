import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HasuraHttpClient extends HttpClient {

  constructor(handler: HttpHandler) {
    super(handler);
  }

  sendRequest(query: string, options: any): Observable<any> {
    return this.post(environment.apiUrl, JSON.stringify({ query }), options);
  }
}
