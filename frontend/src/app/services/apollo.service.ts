import { Injectable } from '@angular/core';
import { Apollo, QueryRef, gql } from 'apollo-angular';
import { Subject } from 'rxjs';
import { Item } from './item.service';

const ITEMS_SUBSCRIPTION = gql`subscription { notes { description id title type } }`;
const ITEMS_QUERY = gql`query { notes { description id title type } }`;

@Injectable({
  providedIn: 'root'
})
export class ApolloService {
  items$: Subject<Item[]> = new Subject<Item[]>();
  private readonly itemsQuery: QueryRef<any>;

  constructor(private apollo: Apollo) {
    this.itemsQuery = apollo.watchQuery({
      query: ITEMS_QUERY
    });
  }

  makeSubscriptionOnItems(): void {
    this.itemsQuery.subscribeToMore({
      document: ITEMS_SUBSCRIPTION,
      updateQuery: (prev, {subscriptionData}) => {
        if (!subscriptionData.data) {
          return prev;
        }
        this.items$.next(subscriptionData.data.notes);
      }
    });
  }
}
