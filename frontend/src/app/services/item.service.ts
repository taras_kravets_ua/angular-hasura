import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HasuraHttpClient } from './hasura-http-client.service';
import { environment } from '../../environments/environment';

export interface Types {
  key: string;
  value: string;
}

export interface Item {
  id: string;
  title: string;
  description: string;
  type: 'ADM' | 'USR';
}

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private readonly queries = {
    types: '{types {key value}}',
    items: '{ notes { description id title type } }'
  };
  private readonly options = {
    headers: {'x-hasura-admin-secret': environment.hasuraAdminSecret}
  };

  constructor(private hasuraHttpClient: HasuraHttpClient) {}

  getTypes(): Observable<Types[]> {
    return this.hasuraHttpClient.sendRequest(this.queries.types, this.options).pipe(
      map((response: any) => response.data.types)
    );
  }

  createItem(item: Item): Observable<any> {
    return this.hasuraHttpClient.sendRequest(this.buildMutationQuery(item), this.options);
  }

  deleteItemById(item: Item): Observable<any> {
    return this.hasuraHttpClient.sendRequest(this.buildDeleteMutationQuery(item), this.options);
  }

  private buildDeleteMutationQuery(item: Item): string {
    return `mutation {
              delete_notes(where: {id: {_eq: "${item.id}"}}) {
                affected_rows
              }
            }`;
  }

  private buildMutationQuery(item: Item): string {
    return `mutation {
              insert_notes(
                objects: {
                  id: "${item.id}",
                  title: "${item.title}",
                  description: "${item.description}",
                  type: ${item.type}
                }
              ) {
                affected_rows
              }
            }`;
  }
}
