import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  private readonly SUCCESS_DURATION_TIME = 700;
  private readonly ERROR_DURATION_TIME = 0;

  constructor(private snackBar: MatSnackBar) { }

  showMessage(message: string, action = ''): void {
    this.snackBar.open(message, action, {
      duration: action ? this.ERROR_DURATION_TIME : this.SUCCESS_DURATION_TIME,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
