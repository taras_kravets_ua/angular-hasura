import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Item } from '../../../services/item.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
  @Input() data!: Item[];
  @Output() removedItem: EventEmitter<Item> = new EventEmitter<Item>();
  displayedColumns: string[] = ['id', 'title', 'description', 'type', 'actions'];
  faTrash = faTrash;

  constructor() { }

  removeItem(element: Item): void {
    this.removedItem.emit(element);
  }

}
