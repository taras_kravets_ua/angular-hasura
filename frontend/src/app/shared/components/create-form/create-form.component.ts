import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ItemService, Types } from '../../../services/item.service';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {
  form: FormGroup = new FormGroup({
    id: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required)
  });

  types!: Types[];

  constructor(private itemService: ItemService) { }

  ngOnInit(): void {
    this.itemService.getTypes()
      .subscribe((types: Types[]) => this.types = types);
  }
}
