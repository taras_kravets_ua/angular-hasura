import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { CdkColumnDef } from '@angular/cdk/table';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CardComponent } from './components/card/card.component';
import { CreateFormComponent } from './components/create-form/create-form.component';
import { TableComponent } from './components/table/table.component';
import { TypePipe } from '../pipes/type.pipe';



@NgModule({
  declarations: [
    CreateFormComponent,
    CardComponent,
    TableComponent,
    TypePipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTableModule,
    FontAwesomeModule,
  ],
  exports: [
    CardComponent,
    MatFormFieldModule,
    MatInputModule,
    TableComponent,
    TypePipe
  ],
  providers: [CdkColumnDef]
})
export class SharedModule { }
