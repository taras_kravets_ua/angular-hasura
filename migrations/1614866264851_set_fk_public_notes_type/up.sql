alter table "public"."notes"
           add constraint "notes_type_fkey"
           foreign key ("type")
           references "public"."type_enum"
           ("value") on update restrict on delete restrict;
