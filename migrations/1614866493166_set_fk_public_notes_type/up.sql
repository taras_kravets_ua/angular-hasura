alter table "public"."notes"
           add constraint "notes_type_fkey"
           foreign key ("type")
           references "public"."types"
           ("key") on update restrict on delete restrict;
